public class find_arry_max
{
    public static void main(String[] args)
    {

        //  test
        int[] arr=new int[]{9,3,5,6,0,1,4,100,9};
        System.out.println(max(arr));
    }


    static int max(int[] arr)
    {
        if(arr.length==0 || arr==null) return -1;
        int i;
        int max=arr[0];
        for(i=0;i<arr.length;i++)
        {
            if(max<=arr[i]) max=arr[i];
        }
        return max;
    }
}
